set nocompatible
filetype off

set mouse=a
set nocp
set nobackup
set ignorecase
set smartcase
set incsearch
set hlsearch
set ruler
set number
set cursorline
set breakindent
set wrap
set linebreak
set backspace=eol,indent,start
set expandtab
set tabstop=4
set shiftwidth=4
set softtabstop=4
set smartindent
set diffopt=filler,iwhite
set showmatch

syntax on

filetype on
filetype plugin on
filetype plugin indent on

set directory=~/.vim/swapfiles

set tags=./tags,tags

let g:netrw_banner = 0
let g:netrw_liststyle = 1
let g:netrw_list_hide = "\.DS_Store"

" quick file browser
nnoremap - :e %:h<CR>

" Jump to buffer
nnoremap gb :ls<CR>:b

set splitbelow
set splitright

" autosave
set updatetime=1000
augroup autosave
    autocmd!
    autocmd BufRead * if &filetype == "" | setlocal ft=text | endif
    autocmd FileType * autocmd TextChanged,InsertLeave <buffer> if &readonly == 0 | silent update | endif
augroup END

colorscheme wildcharm

" LSC (plugin) setup
let g:lsc_auto_map = v:true 
let g:lsc_enable_autocomplete = v:true 
autocmd CompleteDone * silent! pclose
let g:lsc_server_commands = {
\ 'kotlin': {
\    'command': 'path-to/kotlin-language-server',
\    'log_level': 'Warning',
\    'suppress_stderr': v:true,    
\ },
\ 'go': {
\     'command': 'path-to/gopls',
\     'log_level': 'Warning',
\     'suppress_stderr': v:true,
\  },
\ 'c': {
\     'command': 'path-to/clangd',
\     'log_level': 'Warning',
\     'suppress_stderr': v:true,
\ },
\ 'cpp': {
\     'command': 'path-to/clangd',
\     'log_level': 'Warning',
\     'suppress_stderr': v:true,
\ }
\}